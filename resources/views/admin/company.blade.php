@if (count($errors) > 0) 
	@if ($errors->has('name'))
		{{ $errors->first('name') }}
	@endif
@endif

@if (!empty($message))
	{{ $message }}
@endif
<form action="{{ route('companies.store') }}" method="post" >
	Name: <input type="text" name="name"> 
	Description <textarea name="description"></textarea>
	<button type="submit">Save</button>
</form>

@if (!empty($data))
	<table>
		<thead>
			<tr>
				<th>name</th>
				<th>Description</th>
				<th>Owner User</th>
				<th>Updater User</th>
				<th>Create at</th>
				<th>Update at</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($data as $element)
				<tr>
					<th>{{ $element->name }}</th>
					<th>{{ $element->description }}</th>
					<th>{{ $element->ownerUser->name }}</th>
					<th>{{ $element->updaterUser->name }}</th>
					<th>{{ $element->created_at }}</th>
					<th>{{ $element->updated_at }}</th>
					<th>Action</th>
				</tr>
			@endforeach
		</tbody>
	</table>
@endif