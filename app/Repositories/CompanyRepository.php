<?php

namespace CursoLaravel\Repositories;

use CursoLaravel\Company;

class CompanyRepository {

	public function save ($request) {
    Company::create($request->all());
	}

	public function getAll () {
		return Company::all();
	}
}