<?php

namespace CursoLaravel;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Company extends Model {

  protected $table = 'companies';
  protected $dateFormat = 'Y-m-d H:s:i';
  protected $fillable = ['name', 'description', 'is_disabled', 'ip_address', 'owner_user_id', 'updater_user_id'];
  protected $attributes =  [
  	'is_disabled' => 0,
  	'ip_address' => '127.0.0.1',
  	'owner_user_id' => 1,
  	'updater_user_id' => 1
  ];

  public function getCreatedAtAttribute ($value) {
  	if (empty($value)) {
  		return null;
  	}
  	$formatDate = Carbon::createFromFormat($this->dateFormat, $value);
	  return $formatDate->format('m-d-Y g:i:s A');
  }

  public function getUpdatedAtAttribute ($value) {
  	if (empty($value)) {
  		return null;
  	}
  	$formatDate = Carbon::createFromFormat($this->dateFormat, $value);
	  return $formatDate->format('m-d-Y g:i:s A');
  }

  protected function ownerUser () {
		return $this->belongsTo('CursoLaravel\User', 'owner_user_id');
	}

	protected function updaterUser () {
		return $this->belongsTo('CursoLaravel\User', 'updater_user_id');
	}
}
