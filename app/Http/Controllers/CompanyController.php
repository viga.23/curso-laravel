<?php

namespace CursoLaravel\Http\Controllers;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use CursoLaravel\Http\Requests;
use CursoLaravel\Repositories\CompanyRepository;
use CursoLaravel\Http\Requests\CompanyRequest;

class CompanyController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CompanyRepository $company) {
        $data = $company->getAll();
        return view('admin.company', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request, CompanyRepository $company) {
        $company->save($request);
        /*$name = $request->input('name');
        $description = (!empty($request->input('description')) ? $request->input('description') : null);
        $ip = $request->ip();
        $ownerUserId = 1;//$request->user()
        $date = Carbon::now();
        DB::table('companies')->insert([
            'name' => $name,
            'description' => $description,
            'ip_address' => $ip,
            'owner_user_id' => $ownerUserId,
            'updater_user_id' => $ownerUserId
        ]);*/

        return view('admin.company', ['message' => 'The Record was saved successfully']); //redirect()->route('companies.index')->with(['message' => 'The Record was saved successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
