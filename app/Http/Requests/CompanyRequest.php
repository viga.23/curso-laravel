<?php

namespace CursoLaravel\Http\Requests;

use CursoLaravel\Http\Requests\Request;

class CompanyRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|unique:companies,name,null,id,is_disabled,!1'
        ];
    }
}
